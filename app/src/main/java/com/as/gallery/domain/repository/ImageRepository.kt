package com.`as`.gallery.domain.repository

import com.`as`.gallery.domain.model.Image
import io.reactivex.rxjava3.core.Single

interface ImageRepository {
    fun getImages(): Single<List<Image>>
}