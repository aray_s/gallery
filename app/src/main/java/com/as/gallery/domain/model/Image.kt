package com.`as`.gallery.domain.model

data class Image(
    val id: Int,
    val url: String
)
