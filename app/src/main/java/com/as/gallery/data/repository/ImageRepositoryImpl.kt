package com.`as`.gallery.data.repository

import com.`as`.gallery.data.storage.ImageStorage
import com.`as`.gallery.domain.model.Image
import com.`as`.gallery.domain.repository.ImageRepository
import io.reactivex.rxjava3.core.Single

class ImageRepositoryImpl(private val imageStorage: ImageStorage): ImageRepository {
    override fun getImages(): Single<List<Image>> = imageStorage.getImages()
}