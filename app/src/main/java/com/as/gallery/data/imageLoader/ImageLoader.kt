package com.`as`.gallery.data.imageLoader

import android.widget.ImageView

interface ImageLoader {
    fun load(url: String, imageView: ImageView)
}
