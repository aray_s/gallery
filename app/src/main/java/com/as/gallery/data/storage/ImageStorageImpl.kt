package com.`as`.gallery.data.storage

import com.`as`.gallery.data.network.RetrofitServices
import com.`as`.gallery.domain.model.Image
import io.reactivex.rxjava3.core.Single

class ImageStorageImpl(private val apiService: RetrofitServices) : ImageStorage {
    override fun getImages(): Single<List<Image>> = apiService.getImages((1..10).random()).map {
        it.map { dto -> Image(dto.id.toInt(), dto.downloadUrl) }
    }
}