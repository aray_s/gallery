package com.`as`.gallery.data.model

import com.google.gson.annotations.SerializedName

data class ImageDto(
    val id: String,
    @SerializedName("download_url") val downloadUrl: String
)

