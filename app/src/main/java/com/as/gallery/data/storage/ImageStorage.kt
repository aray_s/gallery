package com.`as`.gallery.data.storage

import com.`as`.gallery.domain.model.Image
import io.reactivex.rxjava3.core.Single

interface ImageStorage {
    fun getImages(): Single<List<Image>>
}