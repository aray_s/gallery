package com.`as`.gallery.data.network

import com.`as`.gallery.data.model.ImageDto
import io.reactivex.rxjava3.core.Single
import retrofit2.http.*

interface RetrofitServices {
    @GET("v2/list")
    fun getImages(@Query("page") page: Int): Single<List<ImageDto>>
}
