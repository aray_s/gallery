package com.`as`.gallery.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.`as`.gallery.domain.model.Image
import com.`as`.gallery.domain.usecase.GetImagesUseCase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class MainViewModel(private val getImagesUseCase: GetImagesUseCase) : ViewModel() {

    val images: MutableLiveData<List<Image>> = MutableLiveData()
    val error: MutableLiveData<Boolean> = MutableLiveData()

    private val disposable = CompositeDisposable()

    fun fetchRandomImages() {
        getImagesUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ images.value = it }, { error.value = true })
            .run(disposable::add)
    }

    fun clearError() {
        error.value = false
    }

    override fun onCleared() {
        disposable.dispose()
        super.onCleared()
    }
}
