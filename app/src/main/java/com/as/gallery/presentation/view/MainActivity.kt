package com.`as`.gallery.presentation.view

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.`as`.gallery.databinding.ActivityMainBinding
import com.`as`.gallery.databinding.ItemBinding
import com.`as`.gallery.di.DaggerAppComponent
import com.`as`.gallery.di.GLIDE
import com.`as`.gallery.di.PICASSO
import com.`as`.gallery.data.imageLoader.ImageLoader
import com.`as`.gallery.presentation.view.adapter.ImageAdapter
import com.`as`.gallery.presentation.viewmodel.MainViewModel
import javax.inject.Inject
import javax.inject.Named

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    @Named(GLIDE)
    lateinit var glideImageLoader: ImageLoader
    @Inject
    @Named(PICASSO)
    lateinit var picassoImageLoader: ImageLoader

    private val viewModel by viewModels<MainViewModel> { viewModelFactory }

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: ImageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        DaggerAppComponent.create().inject(this)
        setContentView(binding.root)

        adapter = ImageAdapter(glideImageLoader, ::itemOnClickListener)
        binding.recyclerView.adapter = adapter

        viewModel.fetchRandomImages()
        viewModel.images.observe(this) {
            binding.swipeRefresh.isRefreshing = false
            adapter.updateList(it)
        }

        viewModel.error.observe(this) {
            if (it) {
                binding.swipeRefresh.isRefreshing = false
                Toast.makeText(this, "Load failed", Toast.LENGTH_LONG).show()
                viewModel.clearError()
            }
        }

        binding.swipeRefresh.setOnRefreshListener {
            binding.swipeRefresh.isRefreshing = true
            viewModel.fetchRandomImages()
        }

    }

    private fun itemOnClickListener(url: String) {
        val view = ItemBinding.inflate(layoutInflater)
        AlertDialog.Builder(this).setView(view.root).create().show()
        picassoImageLoader.load(url, view.image)
    }
}
