package com.`as`.gallery.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.`as`.gallery.databinding.ItemBinding
import com.`as`.gallery.domain.model.Image
import com.`as`.gallery.data.imageLoader.ImageLoader

class ImageAdapter(
    private val imageLoader: ImageLoader,
    private val onClickListener: (String) -> Unit
) :
    RecyclerView.Adapter<ImageAdapter.ViewHolder>() {

    private lateinit var binding: ItemBinding
    private var imagesList = mutableListOf<Image>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imageItemUrl = imagesList[position].url
        imageLoader.load(imageItemUrl, holder.image)
        binding.root.setOnClickListener { onClickListener(imageItemUrl) }
    }

    fun updateList(newList: List<Image>) {
        imagesList.clear()
        imagesList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun getItemCount() = imagesList.size

    inner class ViewHolder(binding: ItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val image = binding.image
    }
}