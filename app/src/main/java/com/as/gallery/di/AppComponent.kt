package com.`as`.gallery.di

import com.`as`.gallery.presentation.view.MainActivity
import dagger.Component

@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
}
