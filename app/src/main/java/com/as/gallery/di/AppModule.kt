package com.`as`.gallery.di

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.`as`.gallery.data.network.RetrofitClient
import com.`as`.gallery.data.network.RetrofitServices
import com.`as`.gallery.data.repository.ImageRepositoryImpl
import com.`as`.gallery.data.storage.ImageStorage
import com.`as`.gallery.data.storage.ImageStorageImpl
import com.`as`.gallery.domain.repository.ImageRepository
import com.`as`.gallery.domain.usecase.GetImagesUseCase
import com.`as`.gallery.data.imageLoader.GlideImageLoader
import com.`as`.gallery.data.imageLoader.ImageLoader
import com.`as`.gallery.data.imageLoader.PicassoImageLoader
import com.`as`.gallery.presentation.viewmodel.MainViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Named

const val GLIDE = "glide"
const val PICASSO = "picasso"

@Module
class AppModule {

    @Provides
    fun provideApiService(): RetrofitServices = RetrofitClient.apiService

    @Provides
    fun provideMainViewModelFactory(getImagesUseCase: GetImagesUseCase): ViewModelProvider.Factory {
        return viewModelFactory { initializer { MainViewModel(getImagesUseCase) } }
    }

    @Provides
    fun provideImageStorage(apiService: RetrofitServices): ImageStorage {
        return ImageStorageImpl(apiService)
    }

    @Provides
    fun provideImageRepository(dataStorage: ImageStorage): ImageRepository {
        return ImageRepositoryImpl(dataStorage)
    }

    @Provides
    @Named(GLIDE)
    fun provideGlideImageLoader(glideImageLoader: GlideImageLoader): ImageLoader {
        return glideImageLoader
    }

    @Provides
    @Named(PICASSO)
    fun providePicassoImageLoader(picassoImageLoader: PicassoImageLoader): ImageLoader {
        return picassoImageLoader
    }
}
